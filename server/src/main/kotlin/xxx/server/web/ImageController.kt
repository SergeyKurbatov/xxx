package xxx.server.web

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpHeaders.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import xxx.server.entity.Image
import xxx.server.entity.ImageMeta
import xxx.server.entity.ImageMetaSortField
import xxx.server.entity.Vote
import xxx.server.service.ImageService
import java.util.*
import java.util.Optional.ofNullable

@RestController
@RequestMapping("api/img")
class ImageController(val service: ImageService) {

    @GetMapping
    fun getAllMeta(@RequestParam(name = "order") sortField: Optional<ImageMetaSortField>) =
        service.getAllMeta(sortField)

    @PostMapping
    fun create(@RequestParam("file") file: MultipartFile): ResponseEntity<ImageMeta> {
        val meta = service.create(file.bytes, file.contentType)
        val location = ServletUriComponentsBuilder
            .fromCurrentRequest().path("/{id}")
            .buildAndExpand(meta.id).toUri()
        return created(location).body(meta)
    }

    @GetMapping("/{id}")
    fun getImage(@PathVariable("id") id: Long) =
        mapImage(service.getOne(id))

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteImage(@PathVariable("id") id: Long) =
        service.delete(id)

    @GetMapping("/{id}/thumbnail")
    fun getThumbnail(@PathVariable("id") id: Long) =
        mapImage(service.getOneThumbnail(id))

    @PostMapping("/{id}/vote")
    fun vote(@PathVariable("id") id: Long, @RequestParam(name = "type") vote: Optional<Vote>) =
        mapMeta(service.addVote(id, vote))

    @PostMapping("/{id}/view")
    fun view(@PathVariable("id") id: Long) =
        mapMeta(service.addView(id))

    private fun mapMeta(meta: Optional<ImageMeta>) =
        mapIfExists(meta, { ok(it) })

    private fun mapImage(image: Optional<Image>) =
        mapIfExists(image, {
            ResponseEntity(it.content, HttpHeaders().apply {
                set(CONTENT_TYPE, it.contentType)
                set(CONTENT_LENGTH, it.content.size.toString())
                set(CONTENT_DISPOSITION, "attachment; filename=${it.name}")
            }, HttpStatus.OK)
        })

    private fun <T, R> mapIfExists(item: Optional<T>, mapper: (T) -> ResponseEntity<R>): ResponseEntity<R> =
        item.map(mapper)
            .orElse(notFound().build<R>())
}
