package xxx.server.service

interface ThumbnailCreator {
    fun from(image: ByteArray): ByteArray
}
