package kurbatov_s.xxx

class GlobalProperty private constructor() {

    private val host: String = "http://10.0.2.2:8080/api/"
    private val imageHost: String = "http://10.0.2.2:8080/api/img/"

    fun getHost(): String {
        return host
    }

    fun getImageHost(): String {
        return imageHost
    }

    companion object {

        private var globalProperty: GlobalProperty? = null

        fun getInstance(): GlobalProperty {
            if (null == globalProperty) {
                globalProperty = GlobalProperty()
            }
            return GlobalProperty.globalProperty!!
        }
    }
}
