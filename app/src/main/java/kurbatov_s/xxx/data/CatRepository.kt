package kurbatov_s.xxx.data

import java.util.concurrent.CopyOnWriteArrayList

class CatRepository {

    companion object {

        private val cats: MutableList<Cat> = CopyOnWriteArrayList()

        fun update(list: Iterable<Cat>?) {
            list ?: return
            cats.clear()
            cats.addAll(list)
        }

        fun remove(position: Int) = cats.removeAt(position)

        fun remove(cat: Cat) = cats.remove(cat)

        fun add(cat: Cat) = cats.add(cat)

        fun add(position: Int, cat: Cat) = cats.add(position, cat)

        fun size() = cats.size

        fun get(position: Int) = cats[position]

    }
}
