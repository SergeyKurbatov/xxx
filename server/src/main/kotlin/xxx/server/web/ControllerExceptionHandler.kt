package xxx.server.web

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.multipart.MultipartException
import xxx.server.exception.BadImageException

@ControllerAdvice
class ControllerExceptionHandler {

    val log = LoggerFactory.getLogger(ControllerExceptionHandler::class.java)

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadImageException::class, MultipartException::class, MethodArgumentTypeMismatchException::class)
    fun badClientInputExceptionHandler(e: Exception) =
        log(e)

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception::class)
    fun defaultExceptionHandler(e: Exception) =
        log(e)

    private fun log(e: Exception) {
        log.error("Обработано исключение", e)
    }
}
