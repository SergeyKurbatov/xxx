package kurbatov_s.xxx.utils

import android.content.Intent
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import kurbatov_s.xxx.R

class NavigationUtils {

    companion object {

        fun replaceFragment(manager: FragmentManager?, @LayoutRes container: Int, fragment: Fragment, tag: String) {
            if (manager == null) {
                return
            }
            manager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                .addToBackStack(tag)
                .add(container, fragment, tag)
                .commit()
        }

        fun startImageChooser(activity: AppCompatActivity) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            activity.startActivityForResult(Intent.createChooser(intent, "Choose Image"), 1)
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }
}
