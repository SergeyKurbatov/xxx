package kurbatov_s.xxx.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.StaggeredGridLayoutManager
import android.widget.Toast
import kurbatov_s.xxx.R
import kurbatov_s.xxx.adapter.RecyclerAdapter
import kurbatov_s.xxx.data.Order
import kurbatov_s.xxx.databinding.ActivityMainBinding
import kurbatov_s.xxx.presenter.IRetrofitDataPresenter
import kurbatov_s.xxx.presenter.RetrofitDataPresenter
import kurbatov_s.xxx.utils.NavigationUtils
import kurbatov_s.xxx.utils.PermissionUtils
import kurbatov_s.xxx.utils.PermissionUtils.Companion.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
import kurbatov_s.xxx.utils.Preference
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity(), ISwipeRefreshHandle, IRetrofitDataPresenter {

    lateinit var binding: ActivityMainBinding
    lateinit var adapter: RecyclerAdapter
    lateinit var presenter: RetrofitDataPresenter

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, R.string.permission_error, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val pref = Preference.getInstance(this)
        PermissionUtils.checkPermission(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        startRefresh()

        binding.title.setText(R.string.app_name)
        binding.addPhoto.setOnClickListener({ NavigationUtils.startImageChooser(this) })
        binding.swipeContainer.setOnRefreshListener {
            presenter.requestImages(this, checkOrder(pref))
            stopRefresh()
        }
        binding.createdSorting.setOnClickListener({
            presenter.requestImages(this, Order.CREATED)
            stopRefresh()
        })
        binding.likesSorting.setOnClickListener({
            presenter.requestImages(this, Order.LIKES)
            stopRefresh()
        })

        presenter = RetrofitDataPresenter(this)
        presenter.requestImages(this, checkOrder(pref))
        adapter = RecyclerAdapter(WeakReference(this))
        binding.recyclerView.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        binding.recyclerView.adapter = adapter
    }

    fun checkOrder(pref: Preference): Order {
        return if (!pref.getData("order").isNullOrEmpty() && Order.CREATED.name == pref.getData("order")) Order.CREATED else Order.LIKES
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            presenter.uploadImg(this, data)
            stopRefresh()
        }
    }

    fun deleteImage(id: Long, position: Int) {
        presenter.deleteImages(id, position)
        stopRefresh()
    }

    override fun onRequestSuccess() {
        adapter.notifyDataSetChanged()
        stopRefresh()
    }

    override fun onRequestError() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_SHORT).show()
        stopRefresh()
    }

    override fun startRefresh() {
        binding.swipeContainer.isRefreshing = true
    }

    override fun stopRefresh() {
        binding.swipeContainer.isRefreshing = false
    }
}
