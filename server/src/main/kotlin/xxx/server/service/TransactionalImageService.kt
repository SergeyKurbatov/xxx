package xxx.server.service

import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import xxx.server.entity.ImageMeta
import xxx.server.entity.ImageMetaSortField
import xxx.server.entity.Vote
import xxx.server.exception.BadImageException
import xxx.server.repository.ImageRepository
import java.time.Instant
import java.util.*

@Service
@Transactional
class TransactionalImageService(val repository: ImageRepository,
                                val validator: ImageValidator,
                                val thumbnailCreator: ThumbnailCreator) : ImageService {

    @Cacheable("image")
    @Transactional(readOnly = true)
    override fun getOne(id: Long) =
        repository.findById(id)

    @Cacheable("thumbnail")
    @Transactional(readOnly = true)
    override fun getOneThumbnail(id: Long) =
        repository.findThumbnailById(id)

    @Transactional(readOnly = true)
    override fun getAllMeta(sortField: Optional<ImageMetaSortField>): List<ImageMeta> {
        val sort = sortField.orElseGet { ImageMetaSortField.CREATED }
        when (sort) {
            ImageMetaSortField.CREATED -> return repository.findAllMetaOrderByCreated()
            ImageMetaSortField.LIKES -> return repository.findAllMetaOrderByLikes()
            else -> throw RuntimeException("Unknown sort type: ${sort}")
        }
    }

    override fun addVote(id: Long, type: Optional<Vote>): Optional<ImageMeta> =
        performIfMetaExists(id, {
            val vote = type.orElseGet { Vote.LIKE }
            when (vote) {
                Vote.LIKE -> repository.addLike(id)
                Vote.DISLIKE -> repository.addDislike(id)
                else -> throw RuntimeException("Unknown vote type: ${vote}")
            }
        })

    override fun create(content: ByteArray, type: String): ImageMeta {
        if (!validator.isValid(content, type)) throw BadImageException()
        val thumbnail = thumbnailCreator.from(content)
        val created = Instant.now().toEpochMilli()
        val id = repository.save(content, thumbnail, type, created)
        return ImageMeta(id = id, created = created, likes = 0, dislikes = 0, views = 0)
    }

    override fun addView(id: Long) =
        performIfMetaExists(id, { repository.addView(id) })

    @CacheEvict("image", "thumbnail")
    override fun delete(id: Long) =
        repository.deleteById(id)

    private fun performIfMetaExists(id: Long, performer: () -> Unit): Optional<ImageMeta> =
        repository.findMetaById(id)
            .map {
                performer()
                repository.findMetaById(id)
            }
            .orElse(Optional.empty<ImageMeta>())
}
