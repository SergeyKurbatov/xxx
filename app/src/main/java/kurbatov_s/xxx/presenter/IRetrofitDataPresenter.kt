package kurbatov_s.xxx.presenter

interface IRetrofitDataPresenter {

    fun onRequestSuccess()

    fun onRequestError()

}
