package xxx.server.service

import org.springframework.http.MediaType
import org.springframework.http.MediaType.*
import org.springframework.stereotype.Component
import org.springframework.util.MimeType
import java.util.*
import java.util.function.Predicate

@Component
class MagicBytesImageValidator : ImageValidator {
    override fun isValid(image: ByteArray, type: String): Boolean {
        val mimeType = MimeType.valueOf(type)
        val detector = FileTypeDetectors.values()
            .find { it.mimeType == mimeType }
        return detector != null && detector.isValid(image)
    }

    enum class FileTypeDetectors(val mimeType: MediaType, private val predicate: Predicate<ByteArray>) {
        PNG(IMAGE_PNG, FirstBytesPredicate(arrayOf(byteArrayOf(-119, 80, 78, 71, 13, 10, 26, 10)))),
        JPEG(IMAGE_JPEG, FirstBytesPredicate(arrayOf(byteArrayOf(-1, -40, -1, -32), byteArrayOf(-1, -40, -1, -31)))),
        GIF(IMAGE_GIF, FirstBytesPredicate(arrayOf(byteArrayOf(71, 73, 70))));

        fun isValid(bytes: ByteArray): Boolean {
            return predicate.test(bytes)
        }

        private class FirstBytesPredicate(val detectorsBytes: Array<ByteArray>) : Predicate<ByteArray> {
            override fun test(bytes: ByteArray): Boolean {
                for (detectorBytes in detectorsBytes) {
                    if (bytes.size < detectorBytes.size) {
                        continue
                    }
                    val test = ByteArray(detectorBytes.size)
                    System.arraycopy(bytes, 0, test, 0, detectorBytes.size)
                    if (Arrays.equals(test, detectorBytes)) {
                        return true
                    }
                }
                return false
            }
        }
    }
}
