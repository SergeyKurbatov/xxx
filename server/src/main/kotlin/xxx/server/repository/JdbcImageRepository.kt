package xxx.server.repository

import org.springframework.http.MediaType
import org.springframework.jdbc.core.ResultSetExtractor
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import xxx.server.entity.Image
import xxx.server.entity.ImageMeta
import java.sql.ResultSet
import java.sql.Types.*
import java.util.*
import java.util.Optional.ofNullable

@Repository
class JdbcImageRepository(val jdbcOperations: NamedParameterJdbcOperations) : ImageRepository {

    val idParam = { id: Long -> MapSqlParameterSource().addValue("id", id, BIGINT) }

    val metaMapper = { rs: ResultSet -> ImageMeta(
        id = rs.getLong("id"),
        likes = rs.getLong("likes"),
        dislikes = rs.getLong("dislikes"),
        created = rs.getLong("created"),
        views = rs.getLong("views")
    )}

    override fun findById(id: Long): Optional<Image> {
        val sql = "SELECT id, content, content_type FROM images WHERE id = :id"
        return findOne(sql, id, {
            Image(it.getBytes("content"), it.getLong("id").toString(), it.getString("content_type"))
        })
    }

    override fun findThumbnailById(id: Long): Optional<Image> {
        val sql = "SELECT id, thumbnail FROM images WHERE id = :id"
        return findOne(sql, id, {
            Image(it.getBytes("thumbnail"), it.getLong("id").toString(), MediaType.IMAGE_JPEG_VALUE)
        })
    }

    override fun addLike(id: Long) {
        jdbcOperations.update("UPDATE images SET likes = likes + 1 WHERE id = :id", idParam(id))
    }

    override fun addDislike(id: Long) {
        jdbcOperations.update("UPDATE images SET dislikes = dislikes + 1 WHERE id = :id", idParam(id))
    }

    override fun save(content: ByteArray, thumbnail: ByteArray, contentType: String, created: Long): Long {
        val sql = "INSERT INTO images (content, thumbnail, content_type, created) " +
            "VALUES (:content, :thumbnail, :contentType, :created)"
        val params = MapSqlParameterSource()
            .addValue("contentType", contentType, VARCHAR)
            .addValue("content", content, BINARY)
            .addValue("thumbnail", thumbnail, BINARY)
            .addValue("created", created, BIGINT)
        val keyHolder = GeneratedKeyHolder()
        jdbcOperations.update(sql, params, keyHolder, arrayOf("id"))
        return keyHolder.key.toLong()
    }

    override fun addView(id: Long) {
        jdbcOperations.update("UPDATE images SET views = views + 1 WHERE id = :id", idParam(id))
    }

    override fun findAllMetaOrderByCreated(): List<ImageMeta> =
        findAllMeta("SELECT id, likes, dislikes, created, views FROM images ORDER BY created DESC")

    override fun findAllMetaOrderByLikes(): List<ImageMeta> =
        findAllMeta("SELECT id, likes, dislikes, created, views FROM images ORDER BY likes DESC")

    override fun findMetaById(id: Long): Optional<ImageMeta> {
        val sql = "SELECT id, likes, dislikes, created, views FROM images WHERE id = :id"
        return findOne(sql, id, metaMapper)
    }

    override fun deleteById(id: Long) {
        jdbcOperations.update("DELETE FROM images WHERE id = :id", idParam(id))
    }

    private fun <T> findOne(sql: String, id: Long, mapper: (ResultSet) -> T): Optional<T> =
        ofNullable(jdbcOperations.query(sql, idParam(id), ResultSetExtractor {
            if (it.next()) mapper(it) else null
        }))

    private fun findAllMeta(sql: String): List<ImageMeta> =
        jdbcOperations.query(sql, ResultSetExtractor {
            val list = mutableListOf<ImageMeta>()
            while (it.next()) { list.add(metaMapper(it)) }
            list.toList()
        })
}
