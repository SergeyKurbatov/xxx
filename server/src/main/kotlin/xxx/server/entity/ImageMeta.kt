package xxx.server.entity

class ImageMeta(val id: Long,
                val likes: Long,
                val dislikes: Long,
                val views: Long,
                val created: Long)
