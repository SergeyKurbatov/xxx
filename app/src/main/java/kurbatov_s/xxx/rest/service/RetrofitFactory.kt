package kurbatov_s.xxx.rest.service

import kurbatov_s.xxx.GlobalProperty
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitFactory {

    companion object {

        private val interceptor = HttpLoggingInterceptor()
        private val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        fun <S> createService(serviceClass: Class<S>): S {
            GlobalProperty.getInstance().getHost()
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(GlobalProperty.getInstance().getHost())
                .build()
            return retrofit.create(serviceClass)
        }
    }
}
