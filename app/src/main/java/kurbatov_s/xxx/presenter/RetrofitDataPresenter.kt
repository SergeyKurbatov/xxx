package kurbatov_s.xxx.presenter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kurbatov_s.xxx.data.Cat
import kurbatov_s.xxx.data.CatRepository
import kurbatov_s.xxx.data.Order
import kurbatov_s.xxx.rest.service.IRetrofitService
import kurbatov_s.xxx.rest.service.RetrofitFactory
import kurbatov_s.xxx.utils.FileUtils
import kurbatov_s.xxx.utils.Preference
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response

class RetrofitDataPresenter(val owner: IRetrofitDataPresenter) {

    private val service = RetrofitFactory.createService(IRetrofitService::class.java)

    fun uploadImg(activity: AppCompatActivity, data: Intent?) {
        val file = FileUtils.getFile(activity, data!!.data)

        val requestFile = RequestBody.create(
            MediaType.parse(activity.contentResolver.getType(data.data)), file
        )

        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
        HttpLoggingInterceptor().level = HttpLoggingInterceptor.Level.HEADERS

        val request = service.postImage(body)
        request.enqueue(object : retrofit2.Callback<Cat> {

            override fun onResponse(call: Call<Cat>?, response: Response<Cat>) {
                CatRepository.add(response.body()!!)
                owner.onRequestSuccess()
            }

            override fun onFailure(call: Call<Cat>?, t: Throwable?) {
                Log.e("xxx", "xxx", t)
                owner.onRequestError()
            }
        })
    }

    fun requestImages(activity: AppCompatActivity, order: Order) {
        Preference.getInstance(activity).saveData("order", order.name)
        val response: retrofit2.Call<Set<Cat>> = service.getSortImage(order.name)
        response.enqueue(object : retrofit2.Callback<Set<Cat>> {

            override fun onResponse(call: Call<Set<Cat>>?, response: Response<Set<Cat>>) {
                CatRepository.update(response.body())
                owner.onRequestSuccess()
            }

            override fun onFailure(call: Call<Set<Cat>>?, t: Throwable?) {
                Log.d("yyy", "yyy", t)
                owner.onRequestError()
            }
        })
    }

    fun deleteImages(id: Long, position: Int) {
        val request = service.deleteImage(id)
        request.enqueue(object : retrofit2.Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                CatRepository.remove(position)
                owner.onRequestSuccess()
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.d("yyy", "yyy", t)
                owner.onRequestError()
            }
        })
    }
}
