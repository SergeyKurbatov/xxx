package kurbatov_s.xxx.adapter

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kurbatov_s.xxx.GlobalProperty
import kurbatov_s.xxx.R
import kurbatov_s.xxx.activity.DetailedViewActivity
import kurbatov_s.xxx.activity.MainActivity
import kurbatov_s.xxx.data.Cat
import kurbatov_s.xxx.data.CatRepository
import java.lang.ref.WeakReference


class RecyclerAdapter(val reference: WeakReference<AppCompatActivity>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val view = CatListViewHolder(layoutInflater.inflate(R.layout.item_view_holder, parent, false))
        return view
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as CatListViewHolder).bind(CatRepository.get(position))
    }

    override fun getItemId(position: Int): Long {
        return CatRepository.get(position).id
    }

    override fun getItemCount(): Int {
        return CatRepository.size()
    }

    inner class CatListViewHolder(itemView: View, var item: Cat? = null) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                reference.get()?.startActivity(
                    with(Intent(reference.get(), DetailedViewActivity::class.java)) {
                        putExtra("POSITION", adapterPosition)
                        this
                    })
            }
            itemView.setOnLongClickListener {

                AlertDialog.Builder(itemView.context)
                    .setTitle(itemView.context.getString(R.string.delete_message))
                    .setNegativeButton(itemView.context.getString(R.string.cancel), {
                        dialog, _ ->
                        dialog.dismiss()
                    })
                    .setPositiveButton(itemView.context.getString(R.string.delete), {
                        _, _ ->
                        val a = reference.get() as MainActivity
                        a.deleteImage(itemId, adapterPosition)
                    })
                    .show()
                true
            }
        }

        val photo = itemView.findViewById(R.id.imageView) as ImageView

        fun bind(item: Cat) {
            this.item = item
            Picasso.with(itemView.context)
                .load(GlobalProperty.getInstance().getImageHost() + item.id)
                .into(photo)
        }
    }
}
