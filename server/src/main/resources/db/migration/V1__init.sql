CREATE TABLE images (
    id BIGSERIAL PRIMARY KEY,
    content BYTEA NOT NULL,
    thumbnail BYTEA NOT NULL,
    content_type VARCHAR(30) NOT NULL,
    likes BIGINT NOT NULL DEFAULT 0,
    dislikes BIGINT NOT NULL DEFAULT 0,
    views BIGINT NOT NULL DEFAULT 0,
    created BIGINT NOT NULL
);
