package kurbatov_s.xxx.data

import android.databinding.BaseObservable
import android.databinding.Bindable
import kurbatov_s.xxx.BR

class Cat(val id: Long,
          likes: Long,
          dislikes: Long,
          val views: Long,
          val created: Long): BaseObservable() {


    @Bindable var likes: Long = likes
        set(value) {
            field = value
            notifyPropertyChanged(BR.likes)
        }

    @Bindable var dislikes: Long = dislikes
        set(value) {
            field = value
            notifyPropertyChanged(BR.dislikes)
        }
}
