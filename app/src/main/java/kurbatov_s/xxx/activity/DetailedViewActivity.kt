package kurbatov_s.xxx.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import kurbatov_s.xxx.R
import kurbatov_s.xxx.data.CatRepository
import kurbatov_s.xxx.fragment.DetailedViewFragment
import kurbatov_s.xxx.ui.TestPageTransformer

class DetailedViewActivity : AppCompatActivity() {

    private var viewPagerAdapter: SectionsPagerAdapter? = null

    private var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed_view_activity)

        viewPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        viewPager = findViewById(R.id.container) as ViewPager?
        viewPager?.let {
            it.adapter = viewPagerAdapter
            it.setPageTransformer(false, TestPageTransformer())
            it.currentItem = intent.extras.getInt("POSITION")
        }
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return DetailedViewFragment.createInstance(position)
        }

        override fun getCount(): Int {
            return CatRepository.size()
        }
    }
}
