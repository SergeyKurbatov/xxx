package xxx.server.service

import org.imgscalr.Scalr
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO


@Component
class ScalrThumbnailCreator : ThumbnailCreator {

    override fun from(image: ByteArray): ByteArray {
        val bufImg = ImageIO.read(ByteArrayInputStream(image))
        val thumbnail = Scalr.resize(bufImg,
            Scalr.Method.SPEED,
            Scalr.Mode.FIT_TO_WIDTH,
            150,
            150,
            Scalr.OP_ANTIALIAS)
        val buffer = ByteArrayOutputStream()
        ImageIO.write(thumbnail, "jpg", buffer)
        return buffer.toByteArray()
    }
}
