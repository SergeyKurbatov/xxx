package xxx.server.repository

import xxx.server.entity.Image
import xxx.server.entity.ImageMeta
import xxx.server.entity.ImageMetaSortField
import xxx.server.entity.Vote
import java.util.*

interface ImageRepository {

    fun findById(id: Long): Optional<Image>
    fun findThumbnailById(id: Long): Optional<Image>
    fun findAllMetaOrderByLikes(): List<ImageMeta>
    fun findAllMetaOrderByCreated(): List<ImageMeta>
    fun save(content: ByteArray, thumbnail: ByteArray, contentType: String, created: Long): Long
    fun addView(id: Long)
    fun addLike(id: Long)
    fun addDislike(id: Long)
    fun findMetaById(id: Long): Optional<ImageMeta>
    fun deleteById(id: Long)
}
