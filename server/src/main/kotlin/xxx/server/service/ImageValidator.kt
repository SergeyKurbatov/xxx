package xxx.server.service

interface ImageValidator {

    fun isValid(image: ByteArray, type: String): Boolean
}
