package kurbatov_s.xxx.rest.service

import kurbatov_s.xxx.data.Cat
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IRetrofitService {

    @GET("img")
    fun getSortImage(@Query("order")query: String): Call<Set<Cat>>

    @GET("img")
    fun getImage(): Call<Set<Cat>>

    @Multipart
    @POST("img")
    fun postImage(@Part image: MultipartBody.Part): Call<Cat>

    @POST("img/{id}/vote")
    fun vote(@Path("id") id: Long, @Query("type") vote: String): Call<Cat>

    @DELETE("img/{id}")
    fun deleteImage(@Path("id") id: Long): Call<ResponseBody>
}
