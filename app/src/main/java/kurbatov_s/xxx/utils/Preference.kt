package kurbatov_s.xxx.utils

import android.content.Context
import android.content.SharedPreferences


class Preference private constructor(context: Context) {

    private val sharedPreferences: SharedPreferences? = context.getSharedPreferences("Preference", Context.MODE_PRIVATE)

    companion object {

        private var preference: Preference? = null

        fun getInstance(context: Context): Preference {
            if (preference == null) {
                preference = Preference(context)
            }
            return preference!!
        }
    }

    fun saveData(key: String, value: String) {
        val prefsEditor = sharedPreferences?.edit()
        prefsEditor?.putString(key, value)
        prefsEditor?.apply()
    }

    fun getData(key: String): String? {
        if (sharedPreferences == null) {
            return null
        }
        return sharedPreferences.getString(key, "")
    }
}

