package kurbatov_s.xxx.presenter

interface IDetailedViewPresenter {

    fun onLikeResponse(likes: Long)

    fun onDislikeResponse(dislikes: Long)

}
