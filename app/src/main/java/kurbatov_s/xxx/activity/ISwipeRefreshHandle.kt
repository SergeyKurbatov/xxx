package kurbatov_s.xxx.activity

interface ISwipeRefreshHandle {

    fun startRefresh()

    fun stopRefresh()

}
