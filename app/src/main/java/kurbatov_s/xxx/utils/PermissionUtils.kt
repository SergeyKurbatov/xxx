package kurbatov_s.xxx.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity


class PermissionUtils {

    companion object {

        val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123

        fun checkPermission(context: Context): Boolean {
            val currentAPIVersion = Build.VERSION.SDK_INT
            if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context as AppCompatActivity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE)
                    } else {
                        ActivityCompat.requestPermissions(context, arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE)
                    }
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        }

        fun showDialog(msg: String, context: Context, permission: String) {
            val alertBuilder = AlertDialog.Builder(context)
            alertBuilder.setCancelable(true)
            alertBuilder.setTitle("Permission necessary")
            alertBuilder.setMessage(msg + " permission is necessary")
            alertBuilder.setPositiveButton(android.R.string.yes,
                { _, _ -> ActivityCompat.requestPermissions(context as AppCompatActivity, arrayOf(permission), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) })
            val alert = alertBuilder.create()
            alert.show()
        }
    }
}
