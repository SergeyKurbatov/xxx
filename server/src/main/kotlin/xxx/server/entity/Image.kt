package xxx.server.entity

class Image (val content: ByteArray,
             val name: String,
             val contentType: String)
