package kurbatov_s.xxx.presenter

import android.util.Log
import kurbatov_s.xxx.data.Cat
import kurbatov_s.xxx.data.Vote
import kurbatov_s.xxx.rest.service.IRetrofitService
import kurbatov_s.xxx.rest.service.RetrofitFactory
import retrofit2.Call


class DetailedViewPresenter(private val owner: IDetailedViewPresenter) {

    private val service = RetrofitFactory.createService(IRetrofitService::class.java)

    fun like(cat: Cat) {
        service.vote(cat.id, Vote.LIKE.name).enqueue(enqueue {
            owner.onLikeResponse(it.likes)
        })
    }

    fun dislike(cat: Cat) {
        service.vote(cat.id, Vote.DISLIKE.name).enqueue(enqueue {
            owner.onDislikeResponse(it.dislikes)
        })
    }

    private fun enqueue(xxx: (cat: Cat) -> Unit): retrofit2.Callback<Cat> {
        return object: retrofit2.Callback<Cat> {

            override fun onFailure(call: Call<Cat>?, t: Throwable?) {
                Log.d("zzz", "zzz")
            }

            override fun onResponse(call: Call<Cat>?, response: retrofit2.Response<Cat>) {
                if (response.isSuccessful) {
                    xxx(response.body()!!)
                }
            }
        }
    }
}
