package xxx.server.service

import xxx.server.entity.Image
import xxx.server.entity.ImageMeta
import xxx.server.entity.ImageMetaSortField
import xxx.server.entity.Vote
import java.util.*

interface ImageService {

    fun getOne(id: Long): Optional<Image>
    fun getOneThumbnail(id: Long): Optional<Image>
    fun getAllMeta(sortField: Optional<ImageMetaSortField>): List<ImageMeta>
    fun addVote(id: Long, type: Optional<Vote>): Optional<ImageMeta>
    fun addView(id: Long): Optional<ImageMeta>
    fun create(content: ByteArray, type: String): ImageMeta
    fun delete(id: Long)
}
