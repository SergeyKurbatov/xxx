package kurbatov_s.xxx.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.squareup.picasso.Picasso
import kurbatov_s.xxx.GlobalProperty
import kurbatov_s.xxx.R
import kurbatov_s.xxx.data.CatRepository
import kurbatov_s.xxx.databinding.FragmentDetailedViewActivityBinding
import kurbatov_s.xxx.presenter.DetailedViewPresenter
import kurbatov_s.xxx.presenter.IDetailedViewPresenter

class DetailedViewFragment : Fragment(), IDetailedViewPresenter {

    companion object {

        private val ITEM_KEY = "ITEM_KEY"

        fun createInstance(item: Int?): DetailedViewFragment {
            item ?: throw RuntimeException("Can not show empty image")
            with (DetailedViewFragment()) {
                arguments = Bundle()
                arguments.putInt(ITEM_KEY, item)
                return this
            }
        }
    }

    lateinit var presenter: DetailedViewPresenter
    lateinit var binding: FragmentDetailedViewActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = DetailedViewPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detailed_view_activity, container, false)
        val cat = CatRepository.get(getItem())
        binding.cat = cat
        Picasso.with(context)
            .load(GlobalProperty.getInstance().getImageHost() + cat.id)
            .into(binding.detailedPhoto)
        binding.buttonLike.root.setOnClickListener { presenter.like(cat) }
        binding.buttonDislike.root.setOnClickListener { presenter.dislike(cat) }
        binding.executePendingBindings()

        return binding.root
    }

    private fun getItem(): Int {
        return arguments.getInt(ITEM_KEY)
    }

    override fun onLikeResponse(likes: Long) {
        binding.buttonLike.likeDislikeCount.animate()
            .setDuration(150)
            .alpha(0.5f)
            .setListener(object: AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    binding.cat.likes = likes
                    binding.buttonLike.likeDislikeCount.animate().alpha(1f)
                }
            })
    }

    override fun onDislikeResponse(dislikes: Long) {
        val animationIn = with(AlphaAnimation(1f, 0.5f)) {
            duration = 150
            repeatCount = 1
            repeatMode = Animation.REVERSE
            setAnimationListener(object : Animation.AnimationListener {

                override fun onAnimationRepeat(animation: Animation?) {
                    binding.cat.dislikes = dislikes
                }

                override fun onAnimationEnd(animation: Animation?) {
                }

                override fun onAnimationStart(animation: Animation?) {
                }
            })
            this
        }
        binding.buttonDislike.likeDislikeCount.startAnimation(animationIn)

    }

}
