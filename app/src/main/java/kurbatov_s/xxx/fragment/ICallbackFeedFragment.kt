package kurbatov_s.xxx.fragment

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import kurbatov_s.xxx.data.Order

interface ICallbackFeedFragment {

    fun uploadImg(activity: AppCompatActivity, data: Intent?)

    fun requestImages(order: Order)

    fun deleteImg(id: Long, position: Int)

}
