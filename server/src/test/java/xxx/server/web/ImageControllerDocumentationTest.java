package xxx.server.web;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import xxx.server.entity.Vote;
import xxx.server.repository.ImageRepository;

import java.util.stream.IntStream;

import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.templates.TemplateFormats.asciidoctor;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
public class ImageControllerDocumentationTest {

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        jdbcTemplate.update("DELETE FROM images");
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
            .apply(documentationConfiguration(this.restDocumentation).snippets().withTemplateFormat(asciidoctor())).build();
    }

    @Test
    public void oneExample() throws Exception {
        long id = createImageInDatabase();
        this.mockMvc.perform(get("/api/img/" + id))
            .andExpect(status().isOk())
            .andDo(document("one-example"));
    }

    @Test
    public void oneNotFoundExample() throws Exception {
        this.mockMvc.perform(get("/api/img/1"))
            .andExpect(status().isNotFound())
            .andDo(document("one-not-found-example"));
    }

    @Test
    public void listExample() throws Exception {
        IntStream.rangeClosed(1, 3)
            .forEach(i -> createImageInDatabase());
        listExample("list-example");
    }

    @Test
    public void emptyListExample() throws Exception {
        listExample("empty-list-example");
    }

    @Test
    public void likeExample() throws Exception {
        voteExample("like-example", Vote.LIKE);
    }

    @Test
    public void dislikeExample() throws Exception {
        voteExample("dislike-example", Vote.DISLIKE);
    }

    @Test
    public void viewExample() throws Exception {
        long id = createImageInDatabase();
        this.mockMvc.perform(post("/api/img/" + id + "/view"))
            .andExpect(status().isOk())
            .andDo(document("view-example"));
    }

    private void voteExample(String name, Vote vote) throws Exception {
        long id = createImageInDatabase();
        this.mockMvc.perform(post("/api/img/" + id + "/vote?type=" + vote.name()))
            .andExpect(status().isOk())
            .andDo(document(name));
    }

    private void listExample(String name) throws Exception {
        this.mockMvc.perform(get("/api/img"))
            .andExpect(status().isOk())
            .andDo(document(name));
    }

    private long createImageInDatabase() {
        return imageRepository.save(new byte[0], new byte[0], IMAGE_JPEG_VALUE, 1L);
    }
}
